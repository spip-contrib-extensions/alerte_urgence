<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/alerte_urgence.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_urgence_description' => 'Ce plugin permet de rédiger et d’afficher un message urgent à vos visiteurs. Par défaut il est ajouté en haut de chaque page du site. Il est possible de personnaliser son style et son emplacement.',
	'alerte_urgence_nom' => 'Alerte d’urgence',
	'alerte_urgence_slogan' => 'Afficher un message urgent à vos visiteurs, rapidement et très visible.'
);
