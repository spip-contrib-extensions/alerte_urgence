<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/alerte?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_texte_label' => 'Emergency alert to disseminate:',
	'configurer_titre' => 'Configure the emergency alert',
	'configurer_utilisateurs_explication' => 'By default, only complete administrators can change the alert. Here you can select additional users.',
	'configurer_utilisateurs_label' => 'Additional authorized users'
);
