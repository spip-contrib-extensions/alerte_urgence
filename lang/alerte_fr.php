<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/alerte_urgence.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_desactiver_placement_auto_label' => 'Désactiver l’insertion automatique',
	'configurer_limiter_accueil_label' => 'Limiter l’alerte à la page d’accueil',
	'configurer_texte_label' => 'Alerte d’urgence à diffuser :',
	'configurer_titre' => 'Configurer l’alerte d’urgence',
	'configurer_utilisateurs_explication' => 'Par défaut, seuls les administrateurs complets peuvent modifier l’alerte. On peut sélectionner ici des utilisateurs supplémentaires.',
	'configurer_utilisateurs_label' => 'Utilisateurs supplémentaires autorisés'
);
